
namespace = flavour_bulwar_tag

#Election 1 - head of council
country_event = {
    id = flavour_bulwar_tag.1
    title = flavour_bulwar_tag.1.t
    desc = flavour_bulwar_tag.1.d
    picture = ELECTION_REPUBLICAN_eventPicture
    goto = ROOT
    
    is_triggered_only = yes
    
    trigger = {
		has_reform = bulwar_twelve_families_reform
        NOT = { has_country_flag = in_bulwar_election }
    }
    
    immediate = {
        hidden_effect = {
            set_country_flag = in_bulwar_election
			clr_country_flag = ruqasah_in_government
			clr_country_flag = fula_in_government
			clr_country_flag = kastalukast_in_government
			clr_country_flag = gillugames_in_government
			clr_country_flag = aharmandas_in_government
			clr_country_flag = givar_in_government
			clr_country_flag = bilul_yulun_in_government
			clr_country_flag = husnekar_in_government
			clr_country_flag = alukatran_in_government
			clr_country_flag = lekad_in_government
			clr_country_flag = belatis_in_government
			clr_country_flag = dikuras_in_government
        }
    }
	
	#A Ruqa�ah
    option = {
        name = flavour_bulwar_tag.1.a
        ai_chance = { factor = 8 }
		define_ruler = {
			dynasty = "szel-Ruqa�ah"
			culture = zanite
			change_adm = 2
			change_dip = 0
			change_mil = 0
			max_random_adm = 2
			max_random_dip = 2
			max_random_mil = 2
			min_age = 30
			max_age = 50
			hide_skills = yes
			random_gender = yes
		}
		custom_tooltip = bulwar_tag_elect_pure_adm_tt
		set_ruler_flag = ruqasah_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_ruqasah_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Fula
    option = {
        name = flavour_bulwar_tag.1.b
        ai_chance = { factor = 8 }
		define_ruler = {
			dynasty = "szel-Fula"
			culture = zanite
			change_adm = 2
			change_dip = 0
			change_mil = 0
			max_random_adm = 2
			max_random_dip = 2
			max_random_mil = 2
			min_age = 30
			max_age = 50
			hide_skills = yes
			random_gender = yes
		}
		custom_tooltip = bulwar_tag_elect_pure_adm_tt
		set_ruler_flag = fula_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_fula_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Kastalukast
    option = {
        name = flavour_bulwar_tag.1.c
        ai_chance = { factor = 8 }
		define_ruler = {
			dynasty = "szel-Kastalukast"
			culture = zanite
			change_adm = 1
			change_dip = 1
			change_mil = 0
			max_random_adm = 2
			max_random_dip = 2
			max_random_mil = 2
			min_age = 30
			max_age = 50
			hide_skills = yes
			random_gender = yes
		}
		custom_tooltip = bulwar_tag_elect_adm_dip_tt
		set_ruler_flag = kastalukast_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_kastalukast_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Gillu-game�
    option = {
        name = flavour_bulwar_tag.1.dd
        ai_chance = { factor = 8 }
		define_ruler = {
			dynasty = "szel-Gillu-game�"
			culture = zanite
			change_adm = 1
			change_dip = 1
			change_mil = 0
			max_random_adm = 2
			max_random_dip = 2
			max_random_mil = 2
			min_age = 30
			max_age = 50
			hide_skills = yes
			random_gender = yes
		}
		custom_tooltip = bulwar_tag_elect_adm_dip_tt
		set_ruler_flag = gillugames_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_gillugames_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#An Aharmandas
    option = {
        name = flavour_bulwar_tag.1.e
        ai_chance = { factor = 8 }
		define_ruler = {
			dynasty = "szel-Aharmandas"
			culture = zanite
			change_adm = 0
			change_dip = 2
			change_mil = 0
			max_random_adm = 2
			max_random_dip = 2
			max_random_mil = 2
			min_age = 30
			max_age = 50
			hide_skills = yes
			random_gender = yes
		}
		custom_tooltip = bulwar_tag_elect_pure_dip_tt
		set_ruler_flag = aharmandas_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_aharmandas_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A szal-Givar
    option = {
        name = flavour_bulwar_tag.1.f
        ai_chance = { factor = 8 }
		define_ruler = {
			dynasty = "szel-Givar"
			culture = zanite
			change_adm = 0
			change_dip = 2
			change_mil = 0
			max_random_adm = 2
			max_random_dip = 2
			max_random_mil = 2
			min_age = 30
			max_age = 50
			hide_skills = yes
			random_gender = yes
		}
		custom_tooltip = bulwar_tag_elect_pure_dip_tt
		set_ruler_flag = givar_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_givar_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Bilul-Yulun
    option = {
        name = flavour_bulwar_tag.1.g
        ai_chance = { factor = 8 }
		define_ruler = {
			dynasty = "szel-Bilul-Yulun"
			culture = zanite
			change_adm = 0
			change_dip = 1
			change_mil = 1
			max_random_adm = 2
			max_random_dip = 2
			max_random_mil = 2
			min_age = 30
			max_age = 50
			hide_skills = yes
			random_gender = yes
		}
		custom_tooltip = bulwar_tag_elect_dip_mil_tt
		set_ruler_flag = bilul_yulun_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_bilul_yulun_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Hu�-Nekar
    option = {
        name = flavour_bulwar_tag.1.h
        ai_chance = { factor = 8 }
		define_ruler = {
			dynasty = "szel-Hu�-Nekar"
			culture = zanite
			change_adm = 0
			change_dip = 1
			change_mil = 1
			max_random_adm = 2
			max_random_dip = 2
			max_random_mil = 2
			min_age = 30
			max_age = 50
			hide_skills = yes
			random_gender = yes
		}
		custom_tooltip = bulwar_tag_elect_dip_mil_tt
		set_ruler_flag = husnekar_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_husnekar_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#An Alukaturan
    option = {
        name = flavour_bulwar_tag.1.i
        ai_chance = { factor = 8 }
		define_ruler = {
			dynasty = "szel-Alukaturan"
			culture = zanite
			change_adm = 0
			change_dip = 0
			change_mil = 2
			max_random_adm = 2
			max_random_dip = 2
			max_random_mil = 2
			min_age = 30
			max_age = 50
			hide_skills = yes
			random_gender = yes
		}
		custom_tooltip = bulwar_tag_elect_pure_mil_tt
		set_ruler_flag = alukatran_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_alukatran_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A szal-Lekad
    option = {
        name = flavour_bulwar_tag.1.j
        ai_chance = { factor = 8 }
		define_ruler = {
			dynasty = "szel-Lekad"
			culture = zanite
			change_adm = 0
			change_dip = 0
			change_mil = 2
			max_random_adm = 2
			max_random_dip = 2
			max_random_mil = 2
			min_age = 30
			max_age = 50
			hide_skills = yes
			random_gender = yes
		}
		custom_tooltip = bulwar_tag_elect_pure_mil_tt
		set_ruler_flag = lekad_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_lekad_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Belatis
    option = {
        name = flavour_bulwar_tag.1.k
        ai_chance = { factor = 8 }
		define_ruler = {
			dynasty = "szel-Belatis"
			culture = zanite
			change_adm = 1
			change_dip = 0
			change_mil = 1
			max_random_adm = 2
			max_random_dip = 2
			max_random_mil = 2
			min_age = 30
			max_age = 50
			hide_skills = yes
			random_gender = yes
		}
		custom_tooltip = bulwar_tag_elect_mil_adm_tt
		set_ruler_flag = belatis_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_belatis_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Dikuras
    option = {
        name = flavour_bulwar_tag.1.k
        ai_chance = { factor = 8 }
		define_ruler = {
			dynasty = "szel-Dikuras"
			culture = zanite
			change_adm = 1
			change_dip = 0
			change_mil = 1
			max_random_adm = 2
			max_random_dip = 2
			max_random_mil = 2
			min_age = 30
			max_age = 50
			hide_skills = yes
			random_gender = yes
		}
		custom_tooltip = bulwar_tag_elect_mil_adm_tt
		set_ruler_flag = dikuras_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_dikuras_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	after = {
		country_event = { id = flavour_bulwar_tag.2 }
	}
}

#Election 2 - member of small council
country_event = {
    id = flavour_bulwar_tag.2
    title = flavour_bulwar_tag.2.t
    desc = flavour_bulwar_tag.2.d
    picture = ELECTION_REPUBLICAN_eventPicture
    goto = ROOT
    
    is_triggered_only = yes
	
	#A Ruqa�ah
    option = {
        name = flavour_bulwar_tag.1.a
		trigger = { NOT = { has_ruler_flag = ruqasah_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 2
		hidden_effect = { clear_scripted_personalities = yes }
		random_list = {
			50 = { add_ruler_personality = scholar_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = greedy_personality
			}
		}
		set_ruler_flag = ruqasah_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_ruqasah_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Fula
    option = {
        name = flavour_bulwar_tag.1.b
		trigger = { NOT = { has_ruler_flag = fula_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 2
		hidden_effect = { clear_scripted_personalities = yes }
		random_list = {
			50 = { add_ruler_personality = incorruptible_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = babbling_buffoon_personality
			}
		}
		set_ruler_flag = fula_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_fula_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Kastalukast
    option = {
        name = flavour_bulwar_tag.1.c
		trigger = { NOT = { has_ruler_flag = kastalukast_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 1
		change_dip = 1
		random_list = {
			50 = { add_ruler_personality = kind_hearted_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = loose_lips_personality
			}
		}
		set_ruler_flag = kastalukast_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_kastalukast_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Gillu-game�
    option = {
        name = flavour_bulwar_tag.1.dd
		trigger = { NOT = { has_ruler_flag = gillugames_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 1
		change_dip = 1
		random_list = {
			50 = { add_ruler_personality = secretive_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = craven_personality
			}
		}
		set_ruler_flag = gillugames_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_gillugames_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#An Aharmandas
    option = {
        name = flavour_bulwar_tag.1.e
		trigger = { NOT = { has_ruler_flag = aharmandas_in_government } }
        ai_chance = { factor = 8 }
		change_dip = 2
		random_list = {
			50 = { add_ruler_personality = careful_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = embezzler_personality
			}
		}
		set_ruler_flag = aharmandas_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_aharmandas_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Givar
    option = {
        name = flavour_bulwar_tag.1.f
		trigger = { NOT = { has_ruler_flag = givar_in_government } }
        ai_chance = { factor = 8 }
		change_dip = 2
		random_list = {
			50 = { add_ruler_personality = benevolent_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = drunkard_personality
			}
		}
		set_ruler_flag = givar_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_givar_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Bilul-Yulun
    option = {
        name = flavour_bulwar_tag.1.g
		trigger = { NOT = { has_ruler_flag = bilul_yulun_in_government } }
        ai_chance = { factor = 8 }
		change_dip = 1
		change_mil = 1
		random_list = {
			50 = { add_ruler_personality = architectural_visionary_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = naive_personality
			}
		}
		set_ruler_flag = bilul_yulun_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_bilul_yulun_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Hu�-Nekar
    option = {
        name = flavour_bulwar_tag.1.h
		trigger = { NOT = { has_ruler_flag = husnekar_in_government } }
        ai_chance = { factor = 8 }
		change_dip = 1
		change_mil = 1
		random_list = {
			50 = { add_ruler_personality = industrious_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = obsessive_perfectionist_personality
			}
		}
		set_ruler_flag = husnekar_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_husnekar_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#An Alukaturan
    option = {
        name = flavour_bulwar_tag.1.i
		trigger = { NOT = { has_ruler_flag = alukatran_in_government } }
        ai_chance = { factor = 8 }
		change_mil = 2
		random_list = {
			50 = { add_ruler_personality = intricate_web_weaver_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = malevolent_personality
			}
		}
		set_ruler_flag = alukatran_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_alukatran_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Lekad
    option = {
        name = flavour_bulwar_tag.1.j
		trigger = { NOT = { has_ruler_flag = lekad_in_government } }
        ai_chance = { factor = 8 }
		change_mil = 2
		random_list = {
			50 = { add_ruler_personality = martial_educator_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = obsessive_perfectionist_personality
			}
		}
		set_ruler_flag = lekad_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_lekad_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Belatis
    option = {
        name = flavour_bulwar_tag.1.k
		trigger = { NOT = { has_ruler_flag = belatis_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 1
		change_mil = 1
		random_list = {
			50 = { add_ruler_personality = inspiring_leader_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = malevolent_personality
			}
		}
		set_ruler_flag = belatis_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_belatis_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Dikuras
    option = {
        name = flavour_bulwar_tag.1.k
		trigger = { NOT = { has_ruler_flag = dikuras_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 1
		change_mil = 1
		random_list = {
			50 = { add_ruler_personality = righteous_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = cruel_personality
			}
		}
		set_ruler_flag = dikuras_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_dikuras_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	after = {
		country_event = { id = flavour_bulwar_tag.3 }
	}
}

#Election 2 - member of small council
country_event = {
    id = flavour_bulwar_tag.3
    title = flavour_bulwar_tag.3.t
    desc = flavour_bulwar_tag.3.d
    picture = ELECTION_REPUBLICAN_eventPicture
    goto = ROOT
    
    is_triggered_only = yes
	
	#A Ruqa�ah
    option = {
        name = flavour_bulwar_tag.1.a
		trigger = { NOT = { has_ruler_flag = ruqasah_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 2
		hidden_effect = { clear_scripted_personalities = yes }
		random_list = {
			50 = { add_ruler_personality = scholar_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = greedy_personality
			}
		}
		set_ruler_flag = ruqasah_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_ruqasah_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Fula
    option = {
        name = flavour_bulwar_tag.1.b
		trigger = { NOT = { has_ruler_flag = fula_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 2
		hidden_effect = { clear_scripted_personalities = yes }
		random_list = {
			50 = { add_ruler_personality = incorruptible_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = babbling_buffoon_personality
			}
		}
		set_ruler_flag = fula_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_fula_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Kastalukast
    option = {
        name = flavour_bulwar_tag.1.c
		trigger = { NOT = { has_ruler_flag = kastalukast_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 1
		change_dip = 1
		random_list = {
			50 = { add_ruler_personality = kind_hearted_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = loose_lips_personality
			}
		}
		set_ruler_flag = kastalukast_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_kastalukast_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Gillu-game�
    option = {
        name = flavour_bulwar_tag.1.dd
		trigger = { NOT = { has_ruler_flag = gillugames_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 1
		change_dip = 1
		random_list = {
			50 = { add_ruler_personality = secretive_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = craven_personality
			}
		}
		set_ruler_flag = gillugames_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_gillugames_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#An Aharmandas
    option = {
        name = flavour_bulwar_tag.1.e
		trigger = { NOT = { has_ruler_flag = aharmandas_in_government } }
        ai_chance = { factor = 8 }
		change_dip = 2
		random_list = {
			50 = { add_ruler_personality = careful_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = embezzler_personality
			}
		}
		set_ruler_flag = aharmandas_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_aharmandas_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Givar
    option = {
        name = flavour_bulwar_tag.1.f
		trigger = { NOT = { has_ruler_flag = givar_in_government } }
        ai_chance = { factor = 8 }
		change_dip = 2
		random_list = {
			50 = { add_ruler_personality = benevolent_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = drunkard_personality
			}
		}
		set_ruler_flag = givar_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_givar_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Bilul-Yulun
    option = {
        name = flavour_bulwar_tag.1.g
		trigger = { NOT = { has_ruler_flag = bilul_yulun_in_government } }
        ai_chance = { factor = 8 }
		change_dip = 1
		change_mil = 1
		random_list = {
			50 = { add_ruler_personality = architectural_visionary_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = naive_personality
			}
		}
		set_ruler_flag = bilul_yulun_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_bilul_yulun_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Hu�-Nekar
    option = {
        name = flavour_bulwar_tag.1.h
		trigger = { NOT = { has_ruler_flag = husnekar_in_government } }
        ai_chance = { factor = 8 }
		change_dip = 1
		change_mil = 1
		random_list = {
			50 = { add_ruler_personality = industrious_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = obsessive_perfectionist_personality
			}
		}
		set_ruler_flag = husnekar_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_husnekar_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#An Alukaturan
    option = {
        name = flavour_bulwar_tag.1.i
		trigger = { NOT = { has_ruler_flag = alukatran_in_government } }
        ai_chance = { factor = 8 }
		change_mil = 2
		random_list = {
			50 = { add_ruler_personality = intricate_web_weaver_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = malevolent_personality
			}
		}
		set_ruler_flag = alukatran_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_alukatran_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Lekad
    option = {
        name = flavour_bulwar_tag.1.j
		trigger = { NOT = { has_ruler_flag = lekad_in_government } }
        ai_chance = { factor = 8 }
		change_mil = 2
		random_list = {
			50 = { add_ruler_personality = martial_educator_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = obsessive_perfectionist_personality
			}
		}
		set_ruler_flag = lekad_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_lekad_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Belatis
    option = {
        name = flavour_bulwar_tag.1.k
		trigger = { NOT = { has_ruler_flag = belatis_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 1
		change_mil = 1
		random_list = {
			50 = { add_ruler_personality = inspiring_leader_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = malevolent_personality
			}
		}
		set_ruler_flag = belatis_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_belatis_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Dikuras
    option = {
        name = flavour_bulwar_tag.1.k
		trigger = { NOT = { has_ruler_flag = dikuras_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 1
		change_mil = 1
		random_list = {
			50 = { add_ruler_personality = righteous_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = cruel_personality
			}
		}
		set_ruler_flag = dikuras_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_dikuras_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	after = {
		if = { limit = { has_country_flag = bulwar_tag_four_at_the_council }
			country_event = { id = flavour_bulwar_tag.4 }
		}
		else = {
			clr_country_flag = in_bulwar_election
			clr_country_flag = in_bulwar_election
			clr_country_flag = had_ruqasah_in_government
			clr_country_flag = had_fula_in_government
			clr_country_flag = had_kastalukast_in_government
			clr_country_flag = had_gillugames_in_government
			clr_country_flag = had_aharmandas_in_government
			clr_country_flag = had_givar_in_government
			clr_country_flag = had_bilul_yulun_in_government
			clr_country_flag = had_husnekar_in_government
			clr_country_flag = had_alukatran_in_government
			clr_country_flag = had_lekad_in_government
			clr_country_flag = had_belatis_in_government
			clr_country_flag = had_dikuras_in_government
			trigger_switch = {
				on_trigger = has_ruler_flag
				ruqasah_in_government = { set_country_flag = had_ruqasah_in_government }
				fula_in_government = { set_country_flag = had_fula_in_government }
				kastalukast_in_government = { set_country_flag = had_kastalukast_in_government }
				gillugames_in_government = { set_country_flag = had_gillugames_in_government }
				aharmandas_in_government = { set_country_flag = had_aharmandas_in_government }
				bilul_yulun_in_government = { set_country_flag = had_givar_in_government }
				bilul_yulun_in_government = { set_country_flag = had_bilul_yulun_in_government }
				husnekar_in_government = { set_country_flag = had_husnekar_in_government }
				alukatran_in_government = { set_country_flag = had_alukatran_in_government }
				lekad_in_government = { set_country_flag = had_lekad_in_government }
				belatis_in_government = { set_country_flag = had_belatis_in_government }
				dikuras_in_government = { set_country_flag = had_dikuras_in_government }
			}
		}
	}
}

#Election 2 - member of small council
country_event = {
    id = flavour_bulwar_tag.4
    title = flavour_bulwar_tag.4.t
    desc = flavour_bulwar_tag.4.d
    picture = ELECTION_REPUBLICAN_eventPicture
    goto = ROOT
    
    is_triggered_only = yes
	
	#A Ruqa�ah
    option = {
        name = flavour_bulwar_tag.1.a
		trigger = { NOT = { has_ruler_flag = ruqasah_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 2
		hidden_effect = { clear_scripted_personalities = yes }
		random_list = {
			50 = { add_ruler_personality = scholar_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = greedy_personality
			}
		}
		set_ruler_flag = ruqasah_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_ruqasah_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Fula
    option = {
        name = flavour_bulwar_tag.1.b
		trigger = { NOT = { has_ruler_flag = fula_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 2
		hidden_effect = { clear_scripted_personalities = yes }
		random_list = {
			50 = { add_ruler_personality = incorruptible_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = babbling_buffoon_personality
			}
		}
		set_ruler_flag = fula_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_fula_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Kastalukast
    option = {
        name = flavour_bulwar_tag.1.c
		trigger = { NOT = { has_ruler_flag = kastalukast_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 1
		change_dip = 1
		random_list = {
			50 = { add_ruler_personality = kind_hearted_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = loose_lips_personality
			}
		}
		set_ruler_flag = kastalukast_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_kastalukast_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Gillu-game�
    option = {
        name = flavour_bulwar_tag.1.dd
		trigger = { NOT = { has_ruler_flag = gillugames_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 1
		change_dip = 1
		random_list = {
			50 = { add_ruler_personality = secretive_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = craven_personality
			}
		}
		set_ruler_flag = gillugames_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_gillugames_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#An Aharmandas
    option = {
        name = flavour_bulwar_tag.1.e
		trigger = { NOT = { has_ruler_flag = aharmandas_in_government } }
        ai_chance = { factor = 8 }
		change_dip = 2
		random_list = {
			50 = { add_ruler_personality = careful_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = embezzler_personality
			}
		}
		set_ruler_flag = aharmandas_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_aharmandas_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Givar
    option = {
        name = flavour_bulwar_tag.1.f
		trigger = { NOT = { has_ruler_flag = givar_in_government } }
        ai_chance = { factor = 8 }
		change_dip = 2
		random_list = {
			50 = { add_ruler_personality = benevolent_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = drunkard_personality
			}
		}
		set_ruler_flag = givar_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_givar_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Bilul-Yulun
    option = {
        name = flavour_bulwar_tag.1.g
		trigger = { NOT = { has_ruler_flag = bilul_yulun_in_government } }
        ai_chance = { factor = 8 }
		change_dip = 1
		change_mil = 1
		random_list = {
			50 = { add_ruler_personality = architectural_visionary_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = naive_personality
			}
		}
		set_ruler_flag = bilul_yulun_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_bilul_yulun_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Hu�-Nekar
    option = {
        name = flavour_bulwar_tag.1.h
		trigger = { NOT = { has_ruler_flag = husnekar_in_government } }
        ai_chance = { factor = 8 }
		change_dip = 1
		change_mil = 1
		random_list = {
			50 = { add_ruler_personality = industrious_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = obsessive_perfectionist_personality
			}
		}
		set_ruler_flag = husnekar_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_husnekar_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#An Alukaturan
    option = {
        name = flavour_bulwar_tag.1.i
		trigger = { NOT = { has_ruler_flag = alukatran_in_government } }
        ai_chance = { factor = 8 }
		change_mil = 2
		random_list = {
			50 = { add_ruler_personality = intricate_web_weaver_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = malevolent_personality
			}
		}
		set_ruler_flag = alukatran_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_alukatran_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Lekad
    option = {
        name = flavour_bulwar_tag.1.j
		trigger = { NOT = { has_ruler_flag = lekad_in_government } }
        ai_chance = { factor = 8 }
		change_mil = 2
		random_list = {
			50 = { add_ruler_personality = martial_educator_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = obsessive_perfectionist_personality
			}
		}
		set_ruler_flag = lekad_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_lekad_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Belatis
    option = {
        name = flavour_bulwar_tag.1.k
		trigger = { NOT = { has_ruler_flag = belatis_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 1
		change_mil = 1
		random_list = {
			50 = { add_ruler_personality = inspiring_leader_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = malevolent_personality
			}
		}
		set_ruler_flag = belatis_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_belatis_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	#A Dikuras
    option = {
        name = flavour_bulwar_tag.1.k
		trigger = { NOT = { has_ruler_flag = dikuras_in_government } }
        ai_chance = { factor = 8 }
		change_adm = 1
		change_mil = 1
		random_list = {
			50 = { add_ruler_personality = righteous_personality }
			50 = {
				modifier = { factor = 0.5 has_country_flag = bulwar_tag_rooted_out_bad_apples }
				add_ruler_personality = cruel_personality
			}
		}
		set_ruler_flag = dikuras_in_government
		if = { limit = { NOT = { has_country_flag = bulwar_familial_relations } }
			if = { limit = { has_country_flag = had_dikuras_in_government }
				add_republican_tradition = -3
			}
		}
	}
	
	after = {
		clr_country_flag = in_bulwar_election
		clr_country_flag = had_ruqasah_in_government
		clr_country_flag = had_fula_in_government
		clr_country_flag = had_kastalukast_in_government
		clr_country_flag = had_gillugames_in_government
		clr_country_flag = had_aharmandas_in_government
		clr_country_flag = had_givar_in_government
		clr_country_flag = had_bilul_yulun_in_government
		clr_country_flag = had_husnekar_in_government
		clr_country_flag = had_alukatran_in_government
		clr_country_flag = had_lekad_in_government
		clr_country_flag = had_belatis_in_government
		clr_country_flag = had_dikuras_in_government
		trigger_switch = {
			on_trigger = has_ruler_flag
			ruqasah_in_government = { set_country_flag = had_ruqasah_in_government }
			fula_in_government = { set_country_flag = had_fula_in_government }
			kastalukast_in_government = { set_country_flag = had_kastalukast_in_government }
			gillugames_in_government = { set_country_flag = had_gillugames_in_government }
			aharmandas_in_government = { set_country_flag = had_aharmandas_in_government }
			bilul_yulun_in_government = { set_country_flag = had_givar_in_government }
			bilul_yulun_in_government = { set_country_flag = had_bilul_yulun_in_government }
			husnekar_in_government = { set_country_flag = had_husnekar_in_government }
			alukatran_in_government = { set_country_flag = had_alukatran_in_government }
			lekad_in_government = { set_country_flag = had_lekad_in_government }
			belatis_in_government = { set_country_flag = had_belatis_in_government }
			dikuras_in_government = { set_country_flag = had_dikuras_in_government }
		}
	}
}

